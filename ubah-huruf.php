<?php

function ubah_huruf($string){
//kode di sini
	
	$panjang = strlen($string);
	$arr = str_split($string);

	for ($i = 0; $i < $panjang; $i++) {
	 	$asc = ord($arr[$i]);
	 	$asc = $asc + 1;
	 	echo chr($asc);		
	}
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br><br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br><br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br><br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br><br>";
echo ubah_huruf('semangat'); // tfnbohbu

?>